-----------------------------------------
# UC Davis Extension Coding Boot Camp

#### January 2018 Cohort


-----------------------------------------


## Minimum Requirements


#### Homework


You must complete **90%** of the homework assignments. (You can miss **no more** than **2 assignments**.)


Homework submissions **must be on time AS IS**. Late submissions will not be counted.


#### Attendance


Attendance must be maintained at a **95%** rate. (You can miss **no more** than a total of **4 classes**.)


Written permission must be obtained to miss class or it's considered one of your 4 absences.


#### Projects


You must give a full effort on every group and individual project.


## Weekly Resources

1. 01-html-git-css
	+ Video Guides
		* [Learn to make an HTML page](https://www.youtube.com/watch?v=ieb6Svbc10E)
		* [Learn how to make a basic layout in CSS](https://www.youtube.com/watch?v=kMBinXTCrXI)
		* [Learn how floats and other CSS properties work](https://www.youtube.com/watch?v=0lpxKw6E90Y)
		* [Learn about the positioning property in CSS](https://www.youtube.com/watch?v=sHfJn0jqBro)
		* [Learn to use positioning in a website layout](https://www.youtube.com/watch?v=yWXgnQaWSW0)
	+ Class Videos
		* [01-html-git-css_day 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=916770f3-05ea-4066-9a62-a864002d5f22)
		* [01-html-git-css_day 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=5ebf64ca-93a6-475f-a0d7-a866002a1855)
		* [01-html-git-css_day 3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=bba4d504-a4d6-495b-af67-a8670128e235)
	+ Helpfull Links
		* [Generating an SSH Key for GitHub](https://help.github.com/articles/generating-an-ssh-key/)
		* [Learn Git](https://www.codecademy.com/learn/learn-git)
		* [Try Git](https://try.github.io/levels/1/challenges/1)
		* [Learn CSS Layouts](http://learn.shayhowe.com/html-css/positioning-content/)
		* [Learn CSS Positioning](http://learn.shayhowe.com/advanced-html-css/detailed-css-positioning/)
2. 02-css-bootstrap
	+ Video Guides
		* [Learn how to make your first portfolio using HTML and CSS](https://youtu.be/qMbCiVYQLCU)
		* [Create a basic web layout using Bootstrap](https://www.youtube.com/watch?v=Y9rMbKHoTBI)
		* [Practice building grid-based layouts with the Bootstrap CSS framework](https://www.youtube.com/watch?v=wQovwgW020g)
		* [Learn how to write media queries & make your portfolio responsive](https://www.youtube.com/watch?v=x_wlcp-W27c)
	+ Class Videos
		* [02-css-bootstrap_day 1_part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=7a734a29-b3c4-4931-b9d6-a86d0029419c)
		* [02-css-bootstrap_day 1_part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=d0b9ca68-665d-457d-83e6-a86d0042cf48)
		* [02-css-bootstrap_day 2_part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=69149252-f302-4d71-a7e9-a86e012a9ee7)
		* [02-css-bootstrap_day 2_part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=9cac6483-9701-4a59-90f4-a86e0148fe73)
		* [02-css_bootstrap_day 3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=bba4d504-a4d6-495b-af67-a8670128e235)
	+ Helpfull Links
		* [Github Pages](https://pages.github.com/)
		* [Google Fonts](https://www.google.com/fonts)
		* [Web Design with HTML, CSS, JavaScript, and jQuery Book Set](https://www.amazon.com/Web-Design-HTML-JavaScript-jQuery/dp/1118907442)
		* [Twitter Bootstrap - Official Page](http://getbootstrap.com/)
		* [Twitter Bootstrap - W3Schools](http://www.w3schools.com/bootstrap/bootstrap_get_started.asp)
3. 03-javascript
	+ Video Guides
		* [Learn how to use conditional statements to create a custom website experience based upon user input](https://www.youtube.com/watch?v=rlhhRVO5EOg)
		* [Learn the basics of for-looping through arrays in this simple zoo activity](https://www.youtube.com/watch?v=zJO9g7S2_Xo)
		* [Learn to Solve Problems Algorithmically in JavaScript. Uses Rock, Paper, Scissors algorithm as an example](https://www.youtube.com/watch?v=zRWDJOqeDhg&index=5&list=PLgJ8UgkiorCmEChEWfh7sxPvQwYAx3Kt0)
		* [Learn how to make an interactive game between a user and a computer in the browser - ROCK, PAPER, SCISSORS](https://www.youtube.com/watch?v=Tio88WjwFO0)
		* [Learn about JavaScript objects by making an interactive game](https://www.youtube.com/watch?v=jtU6YrNPv7E&feature=youtu.be)
	+ Class Videos
		* [03-javascript-day 1_part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c5758707-11a9-4568-b156-a874002b066b)
		* [03-javascript-day 1_part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=b2bd9222-f908-4908-b5f9-a874003fce46)
		* 03-javascript-day 2 class video unavailable (due to technical difficulties to record it)
		* [03-javascript-day3 ](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=1d8d3d93-0ae6-4c6b-9e36-a879002a0bd7)
	+ Helpfull Links
		* [Eloquent JavaScript](http://eloquentjavascript.net/)
		* [JavaScript and jQuery](http://www.amazon.com/JavaScript-JQuery-Interactive-Front-End-Development/dp/1118531647/ref=sr_1_1?s=books&ie=UTF8&qid=1460751938&sr=1-1)
4. 04-jQuery
	+ Video Guides
		* [Live coded walkthrough (from scratch) of the hangman game using HTML, Bootstrap CSS, and JavaScript.](https://youtu.be/cgdmOR15cn4)
		* [Learn how to convert basic javascript actions into jQuery](https://www.youtube.com/watch?v=9_9-NeU2L_U)
		* [Learn about click events in jQuery](https://www.youtube.com/watch?v=6BLReDBUZRk)
		* [Build a click-based lottery generatory using JavaScript and jQuery](https://www.youtube.com/watch?v=Nh4wxhzePIs)
		* [Learn how to use jQuery to create and manipulate HTML elements on the page](https://www.youtube.com/watch?v=gC529k3KzmE)
		* [Learn how to use jQuery to create a calculator](https://youtu.be/yKE7016Ioxc)
	+ Class Videos
		* [04-jQuery-day 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=f60a0296-b290-4045-9a6b-a87b00291aad)
		* [04-jQuery-day 2_part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=20fa90d5-54ad-4e4f-b1a9-a87c01295220)
		* [04-jQuery-day 2_part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=95c3973a-fa0e-4d62-8f15-a87c0149a9cd)
		* [04-jQuery-day 3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=8006d66e-9720-4834-8a97-a88000291d83)
	+ Helpfull Links
		* [JavaScript and jQuery](http://www.amazon.com/JavaScript-JQuery-Interactive-Front-End-Development/dp/1118531647/ref=sr_1_1?s=books&ie=UTF8&qid=1460751938&sr=1-1)
		* [What You Should Know About JavaScript Scope](https://spin.atomicobject.com/2014/10/20/javascript-scope-closures/)
5. 05-timers
	+ Video Guides
		* [Live coded walkthrough (from scratch!) of the Crystal Collector game using HTML, Bootstrap CSS, and JavaScript](https://youtu.be/ki36iUBbCDY)
		* [Live coded walkthrough (from scratch!) of the RPG game using HTML, CSS, JavaScript, and jQuery](https://youtu.be/LlHF7o9IFB4)
		* [Learn how to use built in JavaScript functions: setInterval and clearInterval](https://www.youtube.com/watch?v=EGhF4iJSnl0)
		* [Learn how to complete a common technical review challenge: Fizz Buzz](https://www.youtube.com/watch?v=oTart7fFefI)
	+ Class Videos
		* [05-timers-day 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=6e6d951f-6dcd-4fea-b867-a882002adf32)
		* [05-timers-day 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c0983f89-782e-43a9-9039-a883012b5272)
		* [05-timers-day 3-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=2f82260d-63d5-4833-a88c-a88700298434)
		* [05-timers-day 3-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=d258e032-d2c2-4731-8644-a887003f74ff)
	+ Helpfull Links
		* [Eloquent JavaScript](http://eloquentjavascript.net/)
		* [JavaScript Timing Events](http://www.w3schools.com/js/js_timing.asp)
		* [Debug With Breakpoints](https://developers.google.com/web/tools/chrome-devtools/debug/breakpoints/?hl=en)
6. 06-ajax
	+ Video Guides
		* [Walkthrough of how to create a simple/timed trivia game using HTML, JavaScript, and jQuery](https://www.youtube.com/watch?v=3eWhkc_u5rE)
		* [Advanced Totally Trivial Trivia assignment solution](https://www.youtube.com/watch?v=KndV7UxLpnk)
		* [Simple API calls and the basics of AJAX requests](https://www.youtube.com/watch?v=Kp7Xy2LScLM)
		* [Walkthrough of how to attach ajax calls to the clicking of an HTML button](https://www.youtube.com/watch?v=K1JDUkF94cs)
		* [Walkthrough of how to dynamically create HTML elements using jQuery, AJAX, and button clicks](https://www.youtube.com/watch?v=UVBmX4cZkHY)
		* [NYT Search](https://www.youtube.com/watch?v=PDD8NV3sbZo)
	+ Class Videos
		* [06-ajax-day 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=5192362c-3c4b-4a6e-b9e7-a88900296a43)
		* [06-ajax-day 2-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=174858c8-50d1-438a-a8aa-a88a012906a0)
		* [06-ajax-day 2-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=ef853901-33b1-4ddd-98b8-a88a0151bc76)
		* [06-ajax-day 3-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=68f34dde-f575-483d-b6e3-a8900028ab9d)
		* [06-ajax-day 3-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=965afa46-7f50-4b3a-9c1f-a8900041bd68)
	+ Helpfull Links
		* [AJAX Documentation](http://api.jquery.com/jquery.ajax/)
		* [Giphy API](https://developers.giphy.com/docs/)
		* [OMDb API](http://www.omdbapi.com/)
		* [Open Weather Map API](http://openweathermap.org/api)
		* [New York Times Article API](http://developer.nytimes.com/docs/read/article_search_api_v2)
		* [NYT Activity Video Walkthrough (Highly Recommended)](https://youtu.be/RQTVw6XJAac?list=PLgJ8UgkiorCnCFzNp0dP0zJyeFAgstYTj)
7. Firebase
	+ Video Guides
		* [Walkthrough of how to create a simple GIPHY search engine using jQuery, AJAX calls, HTML, and JavaScript](https://www.youtube.com/watch?v=V67yKAonLa4)
		* [Walkthrough of the basics of Firebase while creating a simple web application that counts down from 100 across multiple pages](https://www.youtube.com/watch?v=0PHeP5bLqYE)
		* [Deep dive into Firebase and go over the "set" and "push" methods for adding data to our real-time database](https://www.youtube.com/watch?v=ZWH19t4ujRA)
		* [Walkthrough of creating a simple eBay-like web application using Firebase, jQuery, HTML, and JavaScript](https://www.youtube.com/watch?v=jpSjBZKnrN4)
	+ Class Videos
		* [07-Firebase-day1-part1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=32a529e4-5ad9-4eee-932e-a891012b115b)
		* [07-Firebase-day1-part2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=240c65a9-4c19-4835-bb3d-a8930189b2ec)
		* [07-Firebase-day2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=3b75b72f-5969-4c46-bc84-a89500297d2d)
		* [07-Firebase-day3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=6d5cdbb0-aba9-4711-a1b1-a8970029a012)
	+ Helpfull Links
		* [Firebase Documentation](https://firebase.google.com/docs/)
		* [MomentJS Documentation](http://momentjs.com/)
		* [HTML5 localStorage W3Schools](http://www.w3schools.com/html/html5_webstorage.asp)
8. Project 1 Week
	+ Class Videos
		* [08-Project 1 Week](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=10d6a3a8-c9b0-469c-840c-a8980128cec5)
9. ## PROJECT 1 PRESENTATIONS ##
	+ Videos
		* [Project 1 Presentations Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=e875e6b6-bc77-49cc-8d97-a8a5001b9873)
		* [Project 1 Presentations Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=43027d4e-5f0c-456b-b983-a8a500390313)
10. Nodejs
	+ Video Guides
		* [Train Time homework solution - Train Time is a train scheduler application which utilizes Firebase and Moment.JS to function. Today, we teach you how to make it](https://www.youtube.com/watch?v=Dz5iKzwHi0k&index=9)
		* [Build a command-line based calculator app using Node.js](https://www.youtube.com/watch?v=JH28RCouqfw)
		* [Build a command-line interface (CLI) with the package Inquirer.js](https://www.youtube.com/watch?v=JJqriV7Q9og)
		* [Utilize the Geocoder NPM package to extract location data](https://www.youtube.com/watch?v=G9CtacWgYho)
	+ Class Videos
		* [10-Nodejs-day1-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=391256e7-1b37-4db4-a643-a8a6011a9578)
		* [10-Nodejs-day1-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=ef1a5256-5703-4b2c-8063-a8a601527ba9)
		* [10-Nodejs-day2-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=5ce20361-2fb3-4f27-a37d-a8aa0018c2ea)
		* [10-Nodejs-day2-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c9b749ca-2278-4cc5-bdc8-a8aa003189fd)
		* [10-Nodejs-day3-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=3f4e9dfe-4a83-4d16-a088-a8ac00194639)
	+ Helpfull Links
		* [Basic Intro to Node](https://blog.codeship.com/node-js-tutorial/)
		* [fs Documentation](https://nodejs.org/api/fs.html)
		* [inquirer Documentation](https://www.npmjs.com/package/inquirer)
11.	JavaScript Constructors
	+ Video Guides
		* [LIRI Bot homework solution - LIRI is a Language Interpretation and Recognition Interface - a command line node app that takes in parameters and gives you back data](https://www.youtube.com/watch?v=1-k08YfQbec)
		* [Walkthrough of how to use object constructors whilst creating a small foundation for a basic RPG (roleplaying game)](https://www.youtube.com/watch?v=6_Qi4yg8jQg)
		* [Walkthrough of Inquirer, Constructors, and some of the basics of recursion whilst creating a basic node application](https://www.youtube.com/watch?v=Ph519qG6b9Q)
		* [Walkthrough of how to create a simple Mad Libs application using Node, Inquirer, and the basics of recursion](https://www.youtube.com/watch?v=UNQTZmYk9p0)
	+ Class Videos
		* [11-Constructors-day1-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=3053ba7f-f8bd-4c35-8702-a8ad0118a06a)
		* [11-Constructors-day1-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=17c0bbb4-ab1d-4818-b9f3-a8ad013dad2e)
		* [11-Constructors-day2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=e223c154-1098-458f-a2e7-a8b40119a6f2)
		* [11-Constructors-day3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c4071c07-b041-40e0-9be4-a8b80018e043)
	+ Helpful Links
		* [Recursion in Functional JavaScript](https://www.sitepoint.com/recursion-functional-javascript/)
		* [Understand JavaScript Callbacks](http://javascriptissexy.com/understand-javascript-callback-functions-and-use-them/)
12. MySQL
	+ Video Guides
		* [Dive into MySQL and create your very first database](https://www.youtube.com/watch?v=5tcyKHHbS7U)
		* [Create another eBay like application using Node, Inquirer, and MySQL](https://www.youtube.com/watch?v=oWMAhTUHMFw)
		* [Create a search application that works alongside two databases which contain thousands of pieces of data](https://www.youtube.com/watch?v=CJDp8W9xIVU)
	+ Class Videos
		* [12-MySQL-day1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=b0a56326-9cd5-4ebd-a108-a8ba001901e0)
		* [12-MySQL-day2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=23765226-a9a0-4d27-86cd-a8bb0118fc32)
		* [12-MySQL-day3-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=bb71d051-b0c4-46cc-b85b-a8bf00193812)
		* [12-MySQL-day3-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=72bfbfe4-1398-4f88-96d3-a8bf001bc62e)
		* [12-MySQL-day3-part 3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=025ea0b4-b4e6-4a80-ab70-a8bf003629d1)
	+ Helpfull Links
		* [MySQL - W3Schools](https://www.w3schools.com/sql/)
		* [MySQL Workbench Docs](https://dev.mysql.com/doc/workbench/en/)
		* [MySQL NPM Package](https://www.npmjs.com/package/mysql)
13. Express
	+ Video Guides
		* [Make a basic Amazon-like application for customers using Node, Inquirer, and MySQL](https://www.youtube.com/watch?v=oouxVn14qyk)
		* [Create an app that uses an Express web server. Learn about routing & GET/POST requests](https://www.youtube.com/watch?v=ygk-kNstqK0)
		* [Learn to build a full stack application with node, express, and a custom built API](https://www.youtube.com/watch?v=G7RvQMW2DOg)
	+ Class Videos
		* [13-Express-day1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=db5b5732-f9e6-4c94-a05d-a8c10018d59f)
		* [13-Express-day2-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=b1c6b17d-79cf-4f9a-afb4-a8c201188442)
		* [13-Express-day2-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=f3fa7a99-c9b6-4fb2-b436-a8c2014a28df)
		* [13-Express-day3](git@ucdavis.bootcampcontent.com:ucdavis-boot-camp/UCDVSAC201711FSF2-Class-Repository-FSF.git)
	+ Helpful Links
		* [Postman](https://www.getpostman.com/)
		* [Codeschool's Express Course](https://www.codeschool.com/courses/building-blocks-of-express-js)
		* [Stack Overflow Ports Explanation](https://stackoverflow.com/questions/10182798/why-are-ports-below-1024-privileged)
14. Handlebars
	+ Video Guides
		* [Learn to build a full stack application using node, express, and a custom built API](https://www.youtube.com/watch?v=1mS5w2KOdcQ)
		* [Create a simple Express application for ice cream flavors using Handlebars](https://www.youtube.com/watch?v=cMAIbAJcvZo)
		* [Develop a movie watch list application using Handlebars, MySQL, and Express](https://www.youtube.com/watch?v=qwUbsg95TbI)
		* [Go over the basics of ORM and Handlebars Partials whilst completing the code on a "Sleeping Cats" application](https://www.youtube.com/watch?v=ZooUVmp_t4s)
	+ Class Videos
		* [14-Handlebars-day1-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=b03d17dd-6563-4d65-abd1-a8c8001ba427)
		* [14-Handlebars-day1-part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=066c6564-b112-4eae-8598-a8c80039007c)
		* [14-Handlebars-day2-part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=593de753-c2e1-48fc-ba5a-a8c90118b4bb)
		* 14-Handlebars-day2-part 2 (Failed to record)
	+ Helpful Links
		* [Heroku Deployment Guide for MySQL Projects](./Class-Content/14-handlebars/Supplemental/MySQLHerokuDeploymentProcess.pdf)
		* [Understanding JavaScript Callbacks](http://javascriptissexy.com/understand-javascript-callback-functions-and-use-them/)
		* [Handlebars Website](http://handlebarsjs.com/)
15.	Sequelize
	+ Video Guides
		* [Create an Eat Da Burger application using MySQL, Express, and Node](https://www.youtube.com/watch?v=pieNMQU3oDw)
		* [Learn how to use the Sequelize ORM to create a library with basic CRUD capability](https://www.youtube.com/watch?v=dt9mXaEEAkM)
		* [Learn how to create a blogging application with Sequelize ORM](https://www.youtube.com/watch?v=QmFu_Z_1Dl0)
		* [Learn how to create a blogging application with authors using Sequelize ORM](https://www.youtube.com/watch?v=mrLerceH-tk)
	+ Class Videos
		* [15-Sequelize-day1-part1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=2df2ef93-661f-4163-bae3-a8cf0018c03c)
		* [15-Sequelize-day1-part2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=57a9f3e3-69ee-4ec3-b474-a8cf00370537)
		* [15-Sequelize-day1-part3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=f5a0fc7f-9717-4567-9db3-a8cf00461d6a)
		* [15-Sequelize-day2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=4786e620-232b-4b46-b7ba-a8d0011879cb)
		* [15-Sequelize-day3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=13b7b1dd-7bc7-40fe-babd-a8d40018671a)
	+ Helpful Links
		* [Heroku Deployment Guide for Sequelize Projects](Class-Content/15-sequelize/Supplemental/SequelizeHerokuDeploymentProcess.pdf)
		* [Sequelize Quick Start Guide](Class-Content/15-sequelize/Supplemental/SequelizeQuickStartGuide.pdf)
		* [Sequelize CRUD Actions Cheat Sheet](Class-Content/15-sequelize/Supplemental/SequelizeCRUDActionsCheatSheet.pdf)
		* [Passport and Sequelize Example](Class-Content/15-sequelize/Supplemental/Sequelize-Passport-Example)
		* [Sequelize Queries](http://docs.sequelizejs.com/en/latest/docs/querying/)
		* [Sequelize Associations Part 1](http://docs.sequelizejs.com/en/latest/docs/associations/)
		* [Sequelize Association Part 2](http://docs.sequelizejs.com/en/latest/api/associations/)
		* [Sequelize Migrations](http://docs.sequelizejs.com/en/latest/docs/migrations/)
		* [bcrypt (NPM)](https://www.npmjs.com/package/bcrypt)
		* [Encryption vs Hashing](http://www.securityinnovationeurope.com/blog/whats-the-difference-between-hashing-and-encrypting)
16. Introduction To Testing
	+ Class Videos
		* [Week 16 Day 1 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=211ce12e-9b78-4c9e-aa61-a8d600193670)
		* [Week 16 Day 1 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=45629178-1eca-4d0b-9b58-a8d600408e80)
		* [Week 16 Day 2 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=f8e06228-fecf-4d7a-8c02-a8d701192882)
		* [Week 16 Day 3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=8a1bba11-0ad7-46a2-8503-a8db0018e37e)
	+ Helpful Links
		* [Chai Documentation](http://chaijs.com/)
		* [Writing Testable JavaScript](http://alistapart.com/article/writing-testable-javascript)
		* [What Every Unit Test Needs](https://medium.com/javascript-scene/what-every-unit-test-needs-f6cd34d9836d#.56kpjhvo9)
17. Project 2 Presentations
	+ Videos
		* [Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=067dd4a5-1b62-48a4-858e-a8e400191d97)
		* [Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=d0b11e86-2660-4fe4-9ac3-a8e400309118)
		* Part 3 recording failed and we didn't notice until the end of the presentations :(
18. Mongo-mongoose
	+ Video Guides
		* [Learn how to manipulate data in your mongo database in the terminal](https://www.youtube.com/watch?v=Ci1bui7w75k)
		* [Learn how to connect your mongo data to the front end of your application to display to the user](https://www.youtube.com/watch?v=VFv8J8WbvZM)
		* [Learn how to scrape data from any website into your mongo database using Cheerio](https://www.youtube.com/watch?v=7dTBxMlEVgc)
		* [Learn how to create custom methods in the mongoose ORM for your mongo database](https://www.youtube.com/watch?v=kI4S4Qw1M5Y)
		* [Complete Playlist (Exercises)](https://www.youtube.com/watch?v=Ci1bui7w75k&list=PLgJ8UgkiorCk7zT1kKGwSogEcJbVBzzH8)
		* [Homework Solution Video](https://www.youtube.com/watch?v=17-n9ImiWVc)
	+ Class Videos
		* [Week 18 Day 1 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=51360bd9-4f67-47d8-9c06-a8e5011938be)
		* [Week 18 Day 1 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=f99d67e6-e894-4a9d-8177-a8e5014e40dc)
		* [Week 18 Day 1 Video Guide - Learn how to manipulate data in your mongo database in the terminal!](https://www.youtube.com/watch?v=Ci1bui7w75k)
		* [Week 18 Day 2 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=fddcc907-20a6-4bff-8197-a8e900187583)
		* [Week 18 Day 2 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=e12023f7-0b9c-42f8-9a90-a8e9003a365f)
		* [Week 18 Day 2 Video Guide - MongoJS and the Front End](https://www.youtube.com/watch?v=VFv8J8WbvZM)
		* [Week 18 Day 2 Video Guide - Scraping into your database](https://www.youtube.com/watch?v=7dTBxMlEVgc)
		* [Week 18 Day 3 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=d5150225-66fd-4ee9-9e18-a8eb0018e1e7)
		* [Week 18 Day 3 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=cca7f653-a76d-49e4-b280-a8eb0038d82a)
	+ Helpful Links
		* [MongoDB Website](https://www.mongodb.com/)
		* [RoboMongo Website](https://robomongo.org/download)
		* [MongoJS Documentation](https://www.npmjs.com/package/mongojs)
		* [Cheerio Documentation](https://github.com/cheeriojs/cheerio)
		* [Mongoose Documentation](http://mongoosejs.com/docs/guide.html)
19. React
	+ Video Guides
		* [Learn how to make your first React app using the Create-React-App NPM package](https://youtu.be/gBdgfv8HZYc)
		* [Learn about Props between Parents & Children in React](https://youtu.be/95BauDvEQ6Y)
		* [Combine the use of map, state, and props](https://youtu.be/V32AR5z3gPQ)
	+ Class Videos
		* [Week 19 Day 1 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=7a12f852-f2e7-4f7a-8887-a8f20018e7da)
		* [Week 19 Day 1 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c91af66b-fdc2-48c0-bdef-a8f20037a053)
		* [Week 19 Day 2 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=464296be-ccd3-4be7-8217-a8f3011826fe)
		* [Week 19 Day 2 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=bf0cbcc5-269b-42ea-a020-a8f3014423a8)
		* [Week 19 Day 3](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c32ec0a7-2bbb-4719-861e-a8f70018ee57)
	+ Helpful Links
		* [React Documentation](https://facebook.github.io/react/docs/getting-started.html)
		* [Babel Documentation](https://babeljs.io/docs/setup/#installation)
		* [JSX Documentation](https://facebook.github.io/react/docs/jsx-in-depth.html)
		* [React ES6 Classes](https://facebook.github.io/react/docs/reusable-components.html#es6-classes)
		* [ES6 Cheatsheet](https://github.com/DrkSephy/es6-cheatsheet)
		* [React Router Tutorials](https://github.com/ReactTraining/react-router/tree/master/docs)
		* [React Lifecycle Methods](https://levelup.gitconnected.com/componentdidmakesense-react-lifecycle-explanation-393dcb19e459)
20. More React
	+ Video Guides
		* [Create a simple React application to query the OMDB API and display information about the movie searched for](https://youtu.be/JooyECGSmaE)
		* [Build a complete React application from scratch](https://youtu.be/_l1cwxBLhBY)
		* [Set up a MERN application and make an API call to populate the page](https://youtu.be/RbOkD5M-fV8)
		* [Complete the MERN application started in the last activity by adding functionality for adding and deleting books from the database](https://youtu.be/uBEIj8Z8YEU)
		* [Deploy your first MERN application on Heroku](https://youtu.be/xyWcHBsJbts)
	+ Class Videos
		* [Week 20 Day 1 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=cbb54c64-afbd-4011-8c22-a8f900183913)
		* [Week 20 Day 1 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=8f06c9d5-1db9-4c8e-8eab-a8f9001fdf22)
		* [Week 20 Day 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=c11395f4-7b2c-482e-b627-a8fa012e5747)
		* [Week 20 Day 3 Part 1](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=d1ebadbc-08fe-4650-90f6-a8fe001838af)
		* [Week 20 Day 3 Part 2](https://codingbootcamp.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=94c7331b-2efa-4e5f-abfc-a90000170064)
	+ Helpful Links
		* [React Documentation](https://facebook.github.io/react/docs/getting-started.html)
		* [Babel Documentation](https://babeljs.io/docs/setup/#installation)
		* [JSX Documentation](https://facebook.github.io/react/docs/jsx-in-depth.html)
		* [React ES6 Classes](https://facebook.github.io/react/docs/reusable-components.html#es6-classes)
		* [ES6 Cheatsheet](https://github.com/DrkSephy/es6-cheatsheet)
		* [React Router Tutorials](https://github.com/ReactTraining/react-router/tree/master/docs)
-----------------------------------------


## Important Links And Notes


[Class Slack](https://join.slack.com/t/ucd20180109/shared_invite/enQtMjk0MzUyMDI5NDczLTliYWZmODI1ZmRjNTRjYzNmMmFkYjFjZTFmYTFmMWM2MTg3NjFhZWEwOWM3MWMwY2QzMjdjMTY3MWUwMDQzNmU)

-----------------------------------------


[Homework Submission](http://bootcampspot-v2.com)


Live Office Hours: 45 minutes before class and 30 minutes after class


-----------------------------------------
## Technical Curriculum by Week

Please see [Bootcampspot](https://bootcampspot-v2.com/)

##### The material covered in this syllabus is subject to change. Our academic team adjusts to the market rapidly.

-----------------------------------------
## Career Curriculum By Week

| Week  | Career Content | Career Lesson | Delivery Method | Homework |
| :---: | :--------------| :------------:| :-------------- |:---------| 
| 1   | Setting Expectations for Career Services  | 1  | In class                           | Sign the Student Expectations Document via DocuSign                                                                          |
| 2   | Getting Started with Your Job Search      | 2  | Video                              | Research job opportunities                                                                                                   |
| 4   | Building Your Bio & Perfecting Your Pitch | 3  | Video                              | Write a Bio                                                                                                                  |
| 6-8 | Behavioral Interviewing                   | 4  | Video & Zoom Session to Practice   |                                                                                                                              |
| 10  | Redacting Your Resume                     | 5  | Video                              | Create a Technical Resume                                                                                                    |
| 15  | Creating & Editing Your Online Presence   | 6  | Video                              | Create a Technical LinkedIn profile, Clean GitHub account, and Personal Portfolio                                            |
| 20  | How to Network (Tech Executive Panel)     | 7  | Saturday Event                     |                                                                                                                              |
| 21  | Targeting the Job You Want                | 8  | Video                              | Update all homework assignments with feedback and suggestions and submit final Bio, Resume, LinkedIn, GitHub, and Portfolio  |
| 21  | Career Coaching Lesson 1                  |    | Google Hangout                     |                                                                                                                              |
| 22  | Networking Practice                       | 9  | Individual Student Practice        | Attend a networking event and write about your experience                                                                    |
| 25  | Career Coaching Lesson 2                  |    | Phone Call / Google Hangout        |                                                                                                                              |
|     | Final Project Presentation Day            | 10 | In class, final day of the program |                                                                                                                              | 
| TBD | Graduation Event                          | 11 | In class                           |                                                                                                                              | 

-----------------------------------------



